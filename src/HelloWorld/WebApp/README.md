## How to run

* Build the docker image with:

```
docker build --tag aspnet-core-sample-image .
```

* Run the Docker image with:

```
docker run --detach --publish 8080:80 --rm --name aspnet-core-sample-container aspnet-core-sample-image
```
